function [U,data,S,R] = asap_in_step4(varargin)
 

  debug = 1;
  % parse input
  V = varargin{1};
  F = varargin{2};
  b = varargin{3};
  bc = varargin{4};
  Cf = varargin{5};
  Df = varargin{6};
  Cc = varargin{7};
  Dc = varargin{8};
  w_casap = varargin{9};
  w_f = varargin{10};
  w_c = varargin{11};
  
  G = [];

  % number of vertices
  n = size(V,1);
  assert(isempty(b) || max(b) <= n);
  assert(isempty(b) || min(b) >= 1);

  indices = 1:n;
  max_iterations = 100;
  tol = 0.0001;
  interior = indices(~ismember(indices,b));
  U = [];
  Vm1 = [];
  % default is Sorkine and Alexa style local rigidity energy
  energy = [];
  % default is no external forces
  fext = [];
  % defaults is unit time step
  h = 1;
  % Tikhonov regularization alpha
  alpha_tik = 0;
  % flatten/parameterization
  flat = false;
  % remove rigid transformation invariance
  remove_rigid = false;
  G = [];
  debug = false;
  data = [];
  Aeq = [];
  Beq = [];

  % Map of parameter names to variable names
  params_to_variables = containers.Map( ...
    {'Energy','V0','Data','Tikhonov','Groups','Tol', ...
     'MaxIter','Dynamic','TimeStep','Vm1','Flat','RemoveRigid','Debug', ...
     'Aeq','Beq', 'Smooth'}, ...
    {'energy','U','data','alpha_tik','G','tol','max_iterations','fext', ...
    'h','Vm1','flat','remove_rigid','debug','Aeq','Beq', 'Smooth'});
  v = 12;
  while v <= numel(varargin)
    param_name = varargin{v};
    if isKey(params_to_variables,param_name)
      assert(v+1<=numel(varargin));
      v = v+1;
      % Trick: use feval on anonymous function to use assignin to this workspace 
      feval(@()assignin('caller',params_to_variables(param_name),varargin{v}));
    else
      error('Unsupported parameter: %s',varargin{v});
    end
    v=v+1;
  end

  if isempty(energy)
    switch size(F,2)
    case 4
      energy = 'elements';
    case 3
      energy = 'spokes-and-rims';
    end
  end
  if isempty(fext)
    dynamic = false;
    fext = zeros(size(V));
  else
    dynamic = true;
  end


  if isempty(data)
    if strcmp(energy,'elements') && numel(G) ~= size(F,1) && numel(G) == n
      % groups are defined per vertex, convert to per face using mode
      data.G = mode(G(F),2);
    else
      data.G = G;
    end
  end

  if isempty(data.G)
    k = n;
  else
    k = max(data.G);
  end
  if flat
    [ref_V,ref_F,ref_map] = plane_project(V,F);
    assert(strcmp(energy,'elements'),'flat only makes sense with elements');
  else
    ref_map = 1;
    ref_V = V;
    ref_F = F;
  end
  dim = size(ref_V,2);
  if isempty(bc)
    bc = sparse(0,dim);
  end

  assert(dim == size(bc,2));
  assert(size(Aeq,1) == size(Beq,1));
  assert((size(Aeq,2) == 0) || (size(Aeq,2) == dim*size(V,1)));

  if isempty(U)
    if(dim == 2) 
      U = laplacian_mesh_editing(V,F,b,bc);
    else
      U = V;
    end
    U = U(:,1:dim);
  end
  assert(n == size(U,1));
  assert(dim == size(U,2));

  if dynamic
    V0 = U(:,1:dim);
    if isempty(Vm1)
      Vm1 = V0;
    end
    M = massmatrix(V,F);
    M = M/max(M(:));
    % Larger is more dynamic, smaller is more rigid.
    dw = 5e-2*h^2;
    DQ = dw * 0.5*1/h^2*M;
    vel = (V0-Vm1)/h;
    Dl = dw * (1/(h^2)*M*(-V0 - h*vel) - fext);
    %Dl = 1/h^3*M*(-2*V0 + Vm1) - fext;
  else
    DQ = sparse(size(V,1),size(V,1));
    Dl = sparse(size(V,1),dim);
  end

  if ~isfield(data,'L') || isempty(data.L)
    data.L = cotmatrix(V,F);
  end

  rr.b = cell(dim,1);
  rr.bc = cell(dim,1);
  if ~isfield(data,'rr')
    data.rr = [];
    if ~isfield(data.rr,'preF')
      data.rr.preF = cell(dim,1);
    end
  end
  if ~isfield(data,'preF')
    data.preF = [];
  end
  if remove_rigid
    if ~isempty(b)
      warning('RemoveRigid`s constraints are not typically wanted if |b|>0');
    end
    % the only danger is picking two points which end up mapped very close to
    % each other
    [~,f] = farthest_points(V,dim);
    for c = 1:dim
      rr.b{c} = f([1:dim-(c-1)])';
      rr.bc{c} = zeros(numel(rr.b{c}),1);
    end
    % Immediately remove rigid transformation from initial guess
    U = bsxfun(@minus,U,U(f(1),:));
    % We know that f(1) is at origin
    switch dim
    case 3
      % rotate about y-axis so that f(2) has (x=0)
      theta = atan2(U(f(2),1),U(f(2),3));
      R = axisangle2matrix([0 1 0],theta);
      U = U*R;
      % rotate about x-axis so that f(2) has (y=0)
      theta = atan2(U(f(2),3),U(f(2),2))+pi/2;
      R = axisangle2matrix([1 0 0],theta);
      U = U*R;
      % rotate about z-axis so that f(3) has (x=0)
      theta = atan2(U(f(3),2),U(f(3),1))+pi/2;
      R = axisangle2matrix([0 0 1],theta);
      U = U*R;
    case 2
      % rotate so that f(2) is on y-axis (x=0)
      theta = atan2(U(f(2),2),U(f(2),1))+pi/2;
      R = [cos(theta) -sin(theta);sin(theta) cos(theta)];
      U = U*R;
    otherwise
      error('Unsupported dimension');
    end
  end

  all = [interior b];

  % cholesky factorization
  %cholL = chol(-L(interior,interior),'lower');
  % Why lu and not cholesky?
  %[luL,luU,luP,luQ,luR] = lu(-L(interior,interior));

  %R = repmat(eye(dim,dim),[1 1 n]);

  if ~isfield(data,'ae') || isempty(data.ae)
    data.ae = avgedge(V,F);
  end

    
  %calculate denominator of S
  data.SD = calculate_S(ref_V,ref_V,ref_F,'Energy',energy);
  
  
  
  
  % build covariance scatter matrix used to build covariance matrices we'll
  % later fit rotations to
  if ~isfield(data,'CSM') || isempty(data.CSM)
    assert(size(ref_V,2) == dim);
    data.CSM = covariance_scatter_matrix(ref_V,ref_F,'Energy',energy);
    if flat
      data.CSM = data.CSM * repdiag(ref_map',dim);
    end
    
    % if there are groups then condense scatter matrix to only build
    % covariance matrices for each group
    if ~isempty(G)
      G_sum = group_sum_matrix(data.G,k);
      %CSM = [G_sum sparse(k,n); sparse(k,n) G_sum] * CSM;
      data.CSM = repdiag(G_sum,dim) * data.CSM;
    end
  end
    VV_(:,1:dim) = data.CSM*repmat(V,dim,1);
    % dim by dim by n list of covariance matrices
    VV = permute(reshape(VV_,[size(data.CSM,1)/dim dim dim]),[2 3 1]);
  
  
  % precompute rhs premultiplier
  if ~isfield(data,'K') || isempty(data.K)
    [~,data.K] = arap_rhs(ref_V,ref_F,[],'Energy',energy);
    if flat
      data.K = repdiag(ref_map,dim) * data.K;
    end
  end

  % initialize rotations with identies (not necessary)
  R = repmat(eye(dim,dim),[1 1 size(data.CSM,1)/dim]);
  area = calculate_mesh_area(ref_V, ref_F);
  alpha = 0.02;
  S = ones(size(data.CSM,1)/dim,1);
  %A = triangulation2adjacency(ref_F);
  %A = A./repmat(sum(A,2),1,size(A,2));
  %Adj = diag(sum(A,2)) - A;
  %W = data.L - diag(diag(data.L));
  A = data.L - diag(diag(data.L));
  iteration = 1;
  U_prev = V;
  data.energy = inf;
  spokes_and_rims_binormal = compute_spokes_and_rims_binormal(V,F);
  

  
  
  
  
  while true

    if iteration > max_iterations
      if debug
        fprintf('arap: Iter (%d) > max_iterations (%d)\n',iteration,max_iterations);
      end
      break;
    end

    if iteration > 1
      change = max(abs(U(:)-U_prev(:)));
      if debug
        fprintf('arap: iter: %d, change: %g, energy: %g\n', ...
          iteration,change,data.energy);
      end
      if change <tol*data.ae
        if debug
          fprintf('arap: change (%g) < tol*ae (%g * %g)\n',change,tol,data.ae);
        end
        break;
      end
    end

    U_prev = U;

    % energy after last global step
    U(b,:) = bc;
    %E = arap_energy(V,F,U,R);
    %[1 E]

    %calculate numerator of S
    %if (strcmp(Smooth,'true'))
    %    S_ = realsqrt(  (calculate_S(U,ref_F,'Energy',energy) + alpha * area * (A * (S_(:).^2))) ./ (data.SD + alpha * area*sum(A,2)) ) ; 
    %else

    
    
    %end
    
    % compute covariance matrix elements
    VU_ = zeros(size(data.CSM,1),dim);
    VU_(:,1:dim) = data.CSM*repmat(U,dim,1);
    % dim by dim by n list of covariance matrices
    VU = permute(reshape(VU_,[size(data.CSM,1)/dim dim dim]),[2 3 1]);
    

    
    scale_mat = ones(1,1,size(VU,3)) .* reshape(S, [1 1 size(VU,3)]);
    scale_mat = repmat(scale_mat,[3 3 1]);
    SVU = VU .* scale_mat;
%     if (strcmp(Smooth,'true'))
%         
% 
%         Z = sparse(size(W,1),size(W,2));
%         AA = [  W Z Z  Z Z Z  Z Z Z;
%                 Z W Z  Z Z Z  Z Z Z;
%                 Z Z W  Z Z Z  Z Z Z;
%                 Z Z Z  W Z Z  Z Z Z;
%                 Z Z Z  Z W Z  Z Z Z;
%                 Z Z Z  Z Z W  Z Z Z;
%                 Z Z Z  Z Z Z  W Z Z;
%                 Z Z Z  Z Z Z  Z W Z;
%                 Z Z Z  Z Z Z  Z Z W];
%         RT = permute(R, [2 1 3]);%transpose
% %         S_T = repmat(S_,1,9);
% %         S_T = S_T';
% %         S_T = reshape(S_T, [ dim dim size(data.CSM,1)/dim]);
% %         
% %         RT = RT .* S_T;
%         
%         Rcol = reshape(permute(RT,[3 1 2]),size(data.K,2),1);
%         Rcol = AA * Rcol;
%         SS1 = permute(reshape(Rcol,[size(data.CSM,1)/dim dim dim]),[2 3 1]);
%         %area = calculate_mesh_area(ref_V, ref_F);
%         SS = SS + alpha*area*SS1;
%     end
    
    if (strcmp(Smooth,'true'))
        Z = sparse(size(A,1),size(A,2));
        AA = [  A Z Z  Z Z Z  Z Z Z;
                Z A Z  Z Z Z  Z Z Z;
                Z Z A  Z Z Z  Z Z Z;
                Z Z Z  A Z Z  Z Z Z;
                Z Z Z  Z A Z  Z Z Z;
                Z Z Z  Z Z A  Z Z Z;
                Z Z Z  Z Z Z  A Z Z;
                Z Z Z  Z Z Z  Z A Z;
                Z Z Z  Z Z Z  Z Z A];
        RT = permute(R, [2 1 3]);%transpose      
        Rcol = reshape(permute(RT,[3 1 2]),size(data.K,2),1);
        Rcol = AA * Rcol;
        SS1 = permute(reshape(Rcol,[size(data.CSM,1)/dim dim dim]),[2 3 1]);
        %area = calculate_mesh_area(ref_V, ref_F);
        SVU = SVU + alpha*area*SS1;
        VU = VU + alpha*area*SS1;
    end


    % fit rotations to each deformed vertex
    R = fit_rotations(SVU,'SinglePrecision',false);
%     VT = ref_V';
%     RV = mat3D2blk(R,3) * VT(:);
%     RV = reshape(RV,3,[]);
%     RV = RV';
    % compute scale factor S
    data.SN = calculate_S(U,U,ref_F,'Energy',energy);
    %S_ =  realsqrt(calculate_ASAP_S(U,RV,ref_F,'Energy',energy) ./ calculate_ASAP_S(ref_V,ref_V,ref_F,'Energy',energy)); 
    S1 = realsqrt(data.SN ./ data.SD);
    
    UU_ =  zeros(size(data.CSM,1),dim);   
    UU_(:,1:dim) = covariance_scatter_matrix(U,ref_F,'Energy',energy)*repmat(U,dim,1);
    % dim by dim by n list of covariance matrices
    UU = permute(reshape(UU_,[size(data.CSM,1)/dim dim dim]),[2 3 1]);
    UU_value = zeros(size(data.SN));
    VV_value = zeros(size(data.SN));
    for ii = 1:size(data.CSM,1)/dim
        UU_value(ii) = trace(UU(:,:,ii));
        VV_value(ii) = trace(VV(:,:,ii));
    end    
    
    UU_diff = UU_value - data.SN;
    VV_diff = VV_value - data.SD;
    
    Rcol = reshape(permute(R,[3 1 2]),size(data.K,2),1);
    RMat = reshape(Rcol, 9, n);
    RMat = RMat';
    
    %S = realsqrt(UU_value ./ VV_value);
    for ii = 1:size(data.CSM,1)/dim
        S(ii) = realsqrt(trace(UU(:,:,ii)) / trace(VV(:,:,ii)));
    end

    
    % energy after last local step
    U(b,:) = bc;
    %E = arap_energy(V,F,U,R);
    %[2 E]


    % This is still the SLOW way of building the right hand side, the entire
    % step of building the right hand side should collapse into a
    % #handles*dim*dim+1 by #groups matrix times the rotations at for group
    
    % distribute group rotations to vertices in each group
    if ~isempty(data.G)
      R = R(:,:,data.G);
    end

    U(b,:) = bc;

    
    % ?SV, ???????
    %SV = repmat(S_, 1, 3) .* V;
    
    %B = arap_rhs(V,F,R);
    Rcol = reshape(permute(R,[3 1 2]),size(data.K,2),1);
    S_ = repmat(S, 1, 9);
    S_col = S_(:);
    SRcol = Rcol .* S_col;
    

    
    Bcol = data.K * SRcol;
    %Bcol = data.K * Rcol;
    B = reshape(Bcol,[size(Bcol,1)/dim dim]);

    I = repmat(eye(3),[1 1 size(data.CSM,1)/dim]);
    Icol = reshape(permute(I,[3 1 2]),size(data.K,2),1);
    SScol = Icol .* (S_col.^2);
    
    B1col = data.K * SScol;
    %Bcol = data.K * Rcol;
    B1 = reshape(B1col,[size(B1col,1)/dim dim]);
    
    
  
%     A_ = [w_casap * -data.L; w_f * Cf; w_c * Cc;data.L'*data.L ];
%     b_ = [w_casap * B; w_f * Df; w_c * Dc; zeros(size(U))];
    A_ = [ -data.L;  Cf; Cc;data.L'*data.L ];
    b_ = [ B;  Df;  Dc; zeros(size(U))];
%     A_ = [ -data.L];
%     b_ = [ B];
    
    U =  A_ \ b_;
    
    
%     [U,data.preF] = min_quad_with_fixed( ...
%           0.5*A_, ...
%            -b_,b,bc,Aeq,Beq,data.preF);
    %
    
%     % RemoveRigid requires to solve each indepently
%     if remove_rigid
%       assert(isempty(Aeq),'Linear equality constraints not supported')
%       for c = 1:dim
%         eff_b = [b rr.b{c}];
%         eff_bc = [bc(:,c);rr.bc{c}];
%         [U(:,c),data.rr.preF{c}] = min_quad_with_fixed( ...
%           -0.5*data.L+DQ+alpha_tik*speye(size(data.L)), ...
%           -B(:,c)+Dl(:,c),eff_b,eff_bc,[],[],data.rr.preF{c});
%       end
%     else 
%       if isempty(Aeq)
%         [U,data.preF] = min_quad_with_fixed( ...
%           0.5*A_, ...
%           b_,b,bc,Aeq,Beq,data.preF);
%       else
%         assert(dim == size(bc,2));
%         assert(dim == size(B,2));
%         % repeat diagonal to solve for all coordinates together
%         mQ = repdiag(0.5*A_+DQ+alpha_tik*speye(size(data.L)),dim);
%         mL = reshape(b_+Dl,[],1);
%         mb = reshape(bsxfun(@plus,b(:),size(V,1)*(0:dim-1)),[],1)';
%         mbc = bc(:);
%         Aeq = repdiag(Aeq, dim);
%         Beq = reshape(Beq, [], 1);
%         [U,data.preF] = min_quad_with_fixed(mQ,mL,mb,mbc,Aeq,Beq,data.preF);
%         U = reshape(U,[],dim);
%       end
%     end
    energy_prev = data.energy;
    if strcmp(energy, 'spokes')
        if (strcmp(Smooth,'true'))
            data.energy = trace(U'*(-data.L)*U+2*U'*(-B)+V'*(-data.L*V)) + alpha * area * trace(RMat'*(-data.L)*RMat)
        else
            data.energy = trace(U'*(-data.L)*U+2*U'*(-B)+V'*(-data.L*V))
        end
    elseif strcmp(energy, 'spokes-and-rims') 
        %compute_spokes_and_rims_energy(U,V,F)*Rcol
        if (strcmp(Smooth,'true'))
            %data.energy = trace(U'*spokes_and_rims_binormal*U+V'*spokes_and_rims_binormal*V)-2*compute_spokes_and_rims_energy(U,V,F)*Rcol + alpha * area * trace(RMat'*(-data.L)*RMat)
            %data.energy = trace(U'*(-data.L)*U+2*U'*(-B)+V'*(-data.L*V)) + alpha * area * trace(RMat'*(-data.L)*RMat)
            data.energy = trace(U'*(-data.L)*U+2*U'*(-B) + V'*B1)% + alpha * area * trace(RMat'*(-data.L)*RMat)
            %trace(RMat'*(-data.L)*RMat)
        else
            %data.energy = trace(U'*spokes_and_rims_binormal*U+V'*spokes_and_rims_binormal*V)-2*compute_spokes_and_rims_energy(U,V,F)*Rcol
            %data.energy = trace(U'*(-data.L)*U+2*U'*(-B)+V'*(-data.L*V))
            data.energy = trace(U'*(-data.L)*U+2*U'*(-B) + V'*B1)
            %trace(RMat'*(-data.L)*RMat)
        end
    end
    if data.energy > energy_prev
      if debug
        fprintf('arap: energy (%g) increasing (over %g)\n', ...
          data.energy,energy_prev);
      end
      %break;
    end
    %U(interior,:) = -L(interior,interior) \ (B(interior,:) + L(interior,b)*bc);
    %U(interior,:) = -L(interior,all)*L(all,interior) \ (L(interior,all)*B(all,:) + L(interior,all)*L(all,b)*bc);
    %U(interior,:) = luQ*(luU\(luL\(luP*(luR\(B(interior,:)+L(interior,b)*bc)))));
    %U(interior,:)=cholL\((B(interior,:)+L(interior,b)*bc)'/cholL)';
    iteration = iteration + 1
  end
  U = U(:,1:dim);
end
