function a = calculate_mesh_area(V,F)
a = cross(V(F(:,2),:)-V(F(:,1),:), V(F(:,3),:)-V(F(:,1),:), 2);
a = 0.5 * normrow(a);
a = sum(a);
