function [si sc] = compute_WKS(vertices,faces)

alpha = 2;          % log scalespace basis
T = [1:0.2:20];    % time scales for SI-HKS
Omega = 2:20;       % frequencies for SI-HKS
K = 100;            % number of eigenfunctions

[W A] = mshlp_matrix(vertices,faces);
A = spdiags(A,0,size(A,1),size(A,1));
[evecs, evals] = eigs(W,A,K,'SM');
evals = -diag(evals);
[si sc] = sihks(evecs,evals,alpha,T,Omega); 