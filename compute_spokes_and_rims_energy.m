function E = compute_spokes_and_rims_energy(U,V,F)
  AX = compute_spokes_and_rims_block(U,V,F,1,1);
  BX = compute_spokes_and_rims_block(U,V,F,2,1);
  CX = compute_spokes_and_rims_block(U,V,F,3,1);
  AY = compute_spokes_and_rims_block(U,V,F,1,2);
  BY = compute_spokes_and_rims_block(U,V,F,2,2);
  CY = compute_spokes_and_rims_block(U,V,F,3,2);
  AZ = compute_spokes_and_rims_block(U,V,F,1,3);
  BZ = compute_spokes_and_rims_block(U,V,F,2,3);
  CZ = compute_spokes_and_rims_block(U,V,F,3,3);
  n = size(V,1);
  Z = sparse(n,n);
  E = [ AX BX CX AY BY CY AZ BZ CZ ];
end





  function K = compute_spokes_and_rims_block(U,V,F,du,dv)

      n = size(V,1);  
      % triangles
      C = cotangent(V,F);
      i1 = F(:,1); i2 = F(:,2); i3 = F(:,3);
      I = [ones(size(i1));ones(size(i2));ones(size(i3))];
      J = [i1;i2;i3];
      v = [ ...
          ((U(i2,du)-U(i3,du))+(U(i3,du)-U(i1,du))+(U(i1,du)-U(i2,du))).*(C(:,1).*(V(i2,dv)-V(i3,dv)) + C(:,2).*(V(i3,dv)-V(i1,dv)) + C(:,3).*(V(i1,dv)-V(i2,dv))); ...
          ((U(i2,du)-U(i3,du))+(U(i3,du)-U(i1,du))+(U(i1,du)-U(i2,du))).*(C(:,1).*(V(i2,dv)-V(i3,dv)) + C(:,2).*(V(i3,dv)-V(i1,dv)) + C(:,3).*(V(i1,dv)-V(i2,dv))); ...
          ((U(i2,du)-U(i3,du))+(U(i3,du)-U(i1,du))+(U(i1,du)-U(i2,du))).*(C(:,1).*(V(i2,dv)-V(i3,dv)) + C(:,2).*(V(i3,dv)-V(i1,dv)) + C(:,3).*(V(i1,dv)-V(i2,dv))); ...
        ];
      K = sparse(I,J,v,1,n);
  end
