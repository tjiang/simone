function M = mat3D2blk(A, d)

%   input:
%       A is a 3D d*d*n matrix 
%   output:
%       convert [A1;A2;...;An] into blkdiag([A1;A2;...;An])
%                   A1                       A1
%                   A2                          A2
%                   .                               .
%                   An                                 An
%   created by seamanj on 23/01/2017 in NCCA@UK


M = reshape(permute(A,[1 3 2]), [],d);
M = mat2cell(M,d*ones((size(M,1)/d),1),d);
M = blkdiag(M{:});