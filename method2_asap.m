%%%%% read and display template mesh

obj = 'cactus';

switch(obj)
    case 'cactus'
        off_name = '00-cactus-original.off';
        sel_name = 'cactus.sel';
        def_name = 'cactus.def';   
    case 'cylinder'
       	off_name = '00-cylinder-original.off';
        sel_name = 'cylinder.sel';
        def_name = 'cylinder.def'; 
end
%name = '00-cylinder-original.off';

name = off_name;

options = [];
options.name = name;

[vertex,faces] = read_mesh(name);
vertex = vertex';
faces = faces';


options.face_vertex_color = ones(size(vertex,1),1);
%plot_mesh(vertex, faces, options);

[g0,g1,g2]=read_sel(sel_name);


BI = [g0 g2];
hold on
%scatter3(vertex(BI,1),vertex(BI,2),vertex(BI,3),20,[0 0 1],'filled');
hold off

BC = vertex(BI,:);
E3D = BC(size(g0,2)+1 : size(g0,2)+size(g2,2),:) ;
HOM = [E3D'; ones(1,size(E3D,1))];
A = read_def(def_name);
HOM = A * HOM;
HOM = HOM ./ repmat(HOM(4,:),4,1);
BC(size(g0,2)+1 : size(g0,2)+size(g2,2),:)=HOM(1:3,:)';


U =  asap(vertex,faces,BI,BC,'Energy','spokes-and-rims','Smooth','true');
plot_mesh(U, faces);
view(3);%240 30(plane)
hold on
scatter3(U(BI,1),U(BI,2),U(BI,3),10,[1 0 0],'filled');
hold 