%%%%% read and display template mesh
name = 'square_21.off';%mesh000 has been rotated via meshlab
options = [];
options.name = name;

[vertex,faces] = read_mesh(name);
vertex = vertex';
faces = faces';

plot_mesh(vertex, faces);

BI = [1:42 400:441];
hold on
scatter3(vertex(BI,1),vertex(BI,2),vertex(BI,3),20,[0 0 1],'filled');
hold off

BC = vertex(BI,:);
BC(1:42,:) = BC(1:42,:) + repmat([0 0 0.5], 42 ,1);



U = asap(vertex,faces,BI,BC,'Energy','spokes-and-rims');
plot_mesh(U, faces);

hold on
scatter3(U(BI,1),U(BI,2),U(BI,3),10,[1 0 0],'filled');
hold off