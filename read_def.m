function M = read_def(filename)
  M =   [];
  fp = fopen( filename, 'r' );
  type = fscanf( fp, '%s', 1 );
  while strcmp( type, '' ) == 0
      if strcmp( type, '#' ) == 1
          line = fgets(fp);
          %ignore line
      else
          fseek(fp, -length(type), 0);
          line = fgets(fp);
          line = sscanf(line, '%lf %lf %lf %lf');
          M = [M;line'];          
      end
      type = fscanf( fp, '%s', 1 );
  end