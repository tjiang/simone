function [group0, group1, group2] = read_sel(filename)
group0 = [];
group1 = [];
group2 = [];
  fp = fopen( filename, 'r' );
  type = fscanf( fp, '%s', 1 );
  index = 1;
  while strcmp( type, '' ) == 0
      if strcmp( type, '#' ) == 1
          line = fgets(fp);
          %ignore line
      else
          fseek(fp, -length(type), 0);
          line = fgets(fp);
          group = sscanf(line, '%d');
          switch group
              case 0
                  group0 = [group0 index];
                  index = index + 1;
              case 1
                  group1 = [group1 index];
                  index = index + 1;
              case 2
                  group2 = [group2 index];
                  index = index + 1;
          end
          
      end
      type = fscanf( fp, '%s', 1 );
  end