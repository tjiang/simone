function [face0, face1, face2] = read_sel_to_choose_faces(filename, F)
    [group0, group1, group2] = read_sel(filename);
    face0 = find(sum(ismember(F, group0),2) == 3) - 1;
    face1 = find(sum(ismember(F, group1),2) == 3) - 1;
    face2 = find(sum(ismember(F, group2),2) == 3) - 1;
end