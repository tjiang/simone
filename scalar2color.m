function c = scalar2color(scalar, precision)%precision表示有多少间隙, 

% convert scalar to color vector
%
%
%   Copyright (c) 2017 seamanj@NCCA

if size(scalar,2)>size(scalar,1)
    scalar = scalar';
end
c = zeros(size(scalar,1), 3);
color = jet(precision+1);%共有precision+1个点
index = scalar * precision + 1;

IDX1 = find(index == precision+1);
IDX2 = find(index ~= precision+1);




if ~isempty(IDX1)
    c(IDX1,:) = repmat(color(precision + 1,:),size(IDX1,1),1);
end

if ~isempty(IDX2)
    t = index(IDX2) - floor(index(IDX2));
    c(IDX2,:) = color(floor(index(IDX2)),:) .* repmat((1-t),1,3) + repmat(t,1,3).*color(floor(index(IDX2))+1,:) ;
end


end