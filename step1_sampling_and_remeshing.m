%%%%% read and display template mesh
b_save = 1;





name = 'head-front.obj';%
options = [];
options.name = name;

[vertex,faces] = read_mesh(name);



options.face_vertex_color = repmat([0.37 0.7165 1 ], size(vertex,1),1);
plot_mesh(vertex, faces, options);
%shading interp;
if b_save == 1
    save('vertex','vertex');
    save('faces','faces');
end

 


