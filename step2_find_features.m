function step2_find_features(varargin)

cal = 1;
show_target = 1;
show_template = 1;
show_fine_template = 0;
alpha_ = 0.2;
shading_ = 'flat';

v = 1;

while v <= numel(varargin)
    switch v
        case 1
            show_target =  varargin{v};
        case 2
            show_template = varargin{v};
        case 3
            alpha_ = varargin{v};
        case 4
            shading_ = varargin{v};
        otherwise
            error('Unsupported parameter: %s',varargin{v});
    end
    v=v+1;
end







name = 'head-right.obj';%mesh000 has been rotated via meshlab
options = [];
options.name = name;

[Target_vertex, Target_faces] = read_mesh(name);
%Target_vertex = Target_vertex * 0.01;
%Target_vertex = Target_vertex + repmat([0 -0.9 0], size(Target_vertex,1), 1);

%Target_vertex = Target_vertex';
%

R = roty(-90);
Target_vertex = R*Target_vertex';
Target_vertex = Target_vertex';
Target_vertex = Target_vertex + repmat([1.3 0 -0.72], size(Target_vertex,1), 1);

if cal == 1
    save('Target_vertex','Target_vertex');
    save('Target_faces','Target_faces');
end

load('vertex', 'vertex');
load('faces','faces');
%%%%%  show template and target
hold on
%                                                                                                                   top
% Template_features = [4   320 315 221 237 308 305 338 324 475  508  441  278  432  513  419  271  371  378  296  42  60 386  392  247 244];                                 
% Target_features =   [380 578 503 367 517 519 365 518 713 653 1312 1353 1063 1178 1246 1192 1050 1194 1193 1048 100 797 529 1202 1038 353];
% Template_features=    [371  308 508  475 557 390   392 386 1056 956  60  45 431  232];     
% Target_features =     [1194 519 1312 653 700 543  1202 529 718  421 770  73 1174 500];
%1 left eye ball
%2 right eye ball
%3 left mouth corner
%4 right mouth corner
%5 chin
%6 nose below
%7 left nose corner
%8 right nose corner
%9 back corner
%10 back tip
%11 top left corner
%12 top right corner
%13 left ear
%14 right ear
%                       1   2   3    4    5   6    7    8   9   10  11   12  13  14
Template_features=    [371  308 508  475 557 390          1056     60  45 431  232];     
Target_features =     [1194 519 1312 653 700 543           718      770  73 1174 500];


Template_features = Template_features + 1;
Target_features = Target_features + 1;

%  Template_features = Template_features;
%  Target_features = Target_features;
if show_target == 1
    
    options.shading_type = 'interp';
    options.delete_patch = 'false';
    options.face_vertex_color = repmat([0.37 0.7165 1 ], size(Target_vertex,1),1);
    options = my_plot_mesh(Target_vertex, Target_faces, options);    
    alpha(options.hpatch, alpha_);
    scatter3(Target_vertex(Target_features,1),Target_vertex(Target_features,2),Target_vertex(Target_features,3),20,[1 0 0],'filled');
    
end



if show_template  == 1
    hold on
    options.face_vertex_color = repmat([0.37 0.7165 1 ], size(vertex,1),1);
    options = my_plot_mesh(vertex, faces,options);
    alpha(options.hpatch, 0.2);
    scatter3(vertex(Template_features,1), vertex(Template_features,2), vertex(Template_features,3),20,[0 1 1],'filled'); 
    %scatter3(vertex(Template_fixed,1),vertex(Template_fixed,2),vertex(Template_fixed,3),20,[0 1 0],'filled');
    hold off
end


if cal == 1
    save('Target_features', 'Target_features');
    save('Template_features','Template_features');
end
%left foot  right foot  crotch nose       left hand right hand    
%alpha(alpha_);
if strcmp(shading_, 'interp')
    shading interp;
elseif strcmp( shading_, 'flat')
    shading flat
end
%colormap jet(256);

hold off
% if exist('M')
%     M(end+1) = getframe(gca);  
% end