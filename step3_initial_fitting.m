function step3_initial_fitting
step2_find_features(0,0,0.3,'interp')
b_save = 1;

load('Target_vertex','Target_vertex');
load('Target_faces','Target_faces');

load('Template_features','Template_features');
load('Target_features','Target_features');


vertex = load('vertex','vertex');
vertex = vertex.vertex;

%vertex = unique(vertex, 'rows');

faces = load('faces','faces');
faces = faces.faces;

BI = Template_features;
BC = Target_vertex(Target_features,:);

% BI = [BI Template_fixed];
% BC = [BC; vertex(Template_fixed,:)];
%[v3,~,S3,R3] =  arap(vertex,faces,BI,BC,'Energy','spokes');

i = 1:(size(Template_features,2));
j = Template_features;

v = ones(size(j));
Cf = full(sparse(i,j,v,size(i,2), size(vertex,1)));


%Df = [Target_vertex(Target_features,:);  vertex(Template_fixed,:)];
Df = [Target_vertex(Target_features,:)];



[v3,~,S3,R3] =  asap_in_step3(vertex,faces,BI,BC,Cf,Df,'Energy','spokes-and-rims','Smooth','true');
plot_mesh(v3, faces);
write_obj('v3_mesh.obj', v3, faces);
if b_save == 1
    save('v3','v3');
    save('S3','S3');
    save('R3','R3');
end