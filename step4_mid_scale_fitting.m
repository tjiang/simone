function step4_mid_scale_fitting
load('vertex','vertex');
load('faces','faces');
load('S3','S3');
load('R3','R3');
S4 = S3;
R4 = R3;
debug = 1;
b_cal = 0;
options = [];
step2_find_features(1,0,0.1,'interp');
M(1) = getframe(gca);  
% [options] = my_plot_mesh(coarse_vertex, coarse_faces, options);
% M(2) = getframe(gca);  
load('v3', 'v3');
v4 = v3;
load('Target_vertex','Target_vertex');
load('Target_faces','Target_faces');

load('Template_features','Template_features');
load('Target_features','Target_features');


vertex = load('vertex','vertex');
vertex = vertex.vertex;

faces = load('faces','faces');
faces = faces.faces;

BI = Template_features;
BC = Target_vertex(Target_features,:);

w_casap = 1000;%sqrt(1000);
w_c = 5;%sqrt(10);
w_f = 10000;
D_ = 0.15*norm(max(Target_vertex) - min(Target_vertex));
[Target_normal, ~] = compute_normal(Target_vertex, Target_faces);
Target_normal = Target_normal';
options.normal = Target_normal;
%plot_mesh(Target_vertex,Target_faces, options);



if b_cal
 Target_wks = compute_wks(Target_vertex, Target_faces);
 Target_sihks = compute_sihks(Target_vertex, Target_faces);
%  Target_wks = [];
%  Target_sihks = [];
[~,~,~,~,Target_mc,~,~] = compute_curvature(Target_vertex, Target_faces, options);
else
    Target_wks = [];
    Target_sihks = [];
    Target_mc = [];
end
% 
% 

options.curvature_smoothing = 10;
tau = 1.2;


w_position = 1;
w_normal = 1;
w_mean_curvature = 1;%有用
w_wks = 1;%没用
w_sihks = 1;%没用
w_match = 0;
w_smooth = 1;
Target_descriptor = [w_position*Target_vertex, w_normal*Target_normal, w_mean_curvature*Target_mc, w_wks*Target_wks, w_sihks*Target_sihks];

Target_neighbor = compute_vertex_ring(Target_faces);

debug_index = [ ]; % the indices of debug template vertices;

% shading interp; camlight; colormap jet(256);

options.h_normal = [];
options.show_normal = debug_index;
frame = 2;
while(w_casap > 1)
    if b_cal
     Template_wks = compute_wks(v4, faces);
     Template_sihks = compute_sihks(v4, faces);
%         Template_wks = [];
%         Template_sihks = [];
     options.curvature_smoothing = 10;
    [~,~,~,~,Template_mc,~,~] = compute_curvature(v4, faces, options);
    else
        Template_wks = [];
        Template_sihks = [];
        Template_mc = [];
    end
   
    [v4_normal, ~] = compute_normal(v4, faces);
    v4_normal = v4_normal';
    
    Template_descriptor = [w_position*v4, w_normal*v4_normal, w_mean_curvature*Template_mc, w_wks*Template_wks, w_sihks*Template_sihks];
    
    hold on
    options.normal = v4_normal;
    options.delete_patch = 'true';
    if ~isempty(options.h_normal)
        delete(options.h_normal)
    end
    [options] = my_plot_mesh(v4, faces, options);
    %write_obj(strcat(strcat('v4_mesh',num2str(frame)),'.obj'), v4, faces);
    frame = frame + 1;
    %alpha(options.hpatch, 0.1);
    %colormap jet(256);
    hold off
    IDX_TARGET = [];
    IDX_TEMPLATE = [];
    IDX_TARGET2 = [];
    IDX_TEMPLATE2 = [];
     if w_casap > 30 %以最近点开始找匹配点
    
        [IDX,~] = knnsearch(Target_vertex, v4, 'k',1, 'NSMethod', 'kdtree');%Target_vertex(IDX,:) 为离v4最近的点

        b = sqrt(sum(abs(Target_vertex(IDX,:) - v4).^2,2)) < D_ & dot(Target_normal(IDX,:),v4_normal ,2) > 0 ;%IDX2为500最近pairs中符合条件的pairs
        IDX2 = find(b);

        IDX_TARGET = IDX(IDX2);
        IDX_TEMPLATE = IDX2;
        
    else %法向量要求变高
        [IDX,~] = knnsearch(Target_vertex, v4, 'k',1, 'NSMethod', 'kdtree');%Target_vertex(IDX,:) 为离v4最近的点

        b = sqrt(sum(abs(Target_vertex(IDX,:) - v4).^2,2)) < D_ & dot(Target_normal(IDX,:),v4_normal ,2) > 0.5;%IDX2为500最近pairs中符合条件的pairs
        IDX2 = find(b);

        IDX_TARGET = IDX(IDX2);
        IDX_TEMPLATE = IDX2;

    end

    
    
    
%     for i = 1:size(IDX_TARGET,1)%以feature搜索
%        j = IDX_TARGET(i);
%        k = j;
%        while 1
%            j = k;
%            neighbors = [j Target_neighbor{IDX_TARGET(i)}];
%            n_neighbors = numel(neighbors);
%            d_neighbors = normrow(Target_descriptor(neighbors',:) - repmat(Template_descriptor(IDX_TEMPLATE(i),:),n_neighbors,1));
%            ave_d = sum(d_neighbors) / n_neighbors;
%            E_m = w_match * d_neighbors.^2 + w_smooth*(d_neighbors-repmat(ave_d,n_neighbors,1)).^2;
%            [~,I] = min(E_m,[],1);
%            k = neighbors(I);
%            if k == j
%                break
%            end
%        end
%        IDX_TARGET(i) = j;
%        if debug  & ismember(i, debug_index )
%            if exist('h5')~=0
%                delete(h5);
%            end
%            hold on
%            h5 = scatter3(Target_vertex(j,1),Target_vertex(j,2),Target_vertex(j,3),20,'filled');
%            delete(h5);
%            hold off
%        end
%     end
    
    Dc  = repmat(dot(Target_vertex(IDX_TARGET,:) - v4(IDX_TEMPLATE,:), v4_normal(IDX_TEMPLATE,:),2),1,3).*v4_normal(IDX_TEMPLATE,:)*1 + v4(IDX_TEMPLATE,:);
    
    
    if debug
        num = size(IDX_TARGET, 1); 
        colorvector = (1:num)'/ num; 
        hold on
        if exist('h11')~=0
            delete(h11);
        end
        h11 = scatter3(v4(IDX_TEMPLATE,1),v4(IDX_TEMPLATE,2),v4(IDX_TEMPLATE,3),20,colorvector);  

        if exist('h22')~=0
            delete(h22);
        end
        h22 = scatter3(Target_vertex(IDX_TARGET,1),Target_vertex(IDX_TARGET,2),Target_vertex(IDX_TARGET,3),20,colorvector,'filled'); 
        colormap jet(256);
        %delete(h11);
        %delete(h22);
    end

    
   
    
   
    
    %i = 1:(size(Template_features,2)+size(Template_fixed,2));
    %j = [Template_features Template_fixed];
    
    i = 1:(size(Template_features,2));
    j = [Template_features];
    
    v = ones(size(j));
    Cf = full(sparse(i,j,v,size(i,2), size(v4,1)));
    
    
    %Df = [Target_vertex(Target_features,:);  vertex(Template_fixed,:)];
    Df = [Target_vertex(Target_features,:)];
    

    
    
    

    ii = 1:size(IDX_TEMPLATE,1);
    jj = IDX_TEMPLATE;
    vv = ones(size(IDX_TEMPLATE));
    Cc = full(sparse(ii,jj,vv,size(IDX_TEMPLATE,1), size(v4,1)));
    
    
    [v4,~,S,R] = asap_in_step4(v4,faces,BI,BC,Cf,Df,Cc,Dc,w_casap,w_f,w_c,'Energy','spokes-and-rims','Smooth','true');
    S4 = S4.*S;
    
    %R4 = arrayfun(@(M1,M2,n) M1(:,:,n)*M2(:,:,n), R, R4, size(R4,3) );
    for iii = 1:size(R,3)
    R4(:,:,iii) = R(:,:,iii) * R4(:,:,iii);
    end
     w_casap = floor(w_casap / 1.5);
    
%    if( w_casap > 100)
%         w_casap = floor(w_casap / 1.1);
%    elseif( w_casap > 10)
%         w_casap = w_casap - 5;
%    else
%        w_casap = w_casap - 0.5;
%    end
    
    %w_casap = w_casap - 0.01;
    M(end+1) = getframe(gca);
    fprintf('w_casap = %d\n',  w_casap);
end
plot_mesh(v4, faces);
save('v4','v4');
save('M','M');
save('S4','S4');
save('R4','R4');
write_obj('v4_mesh.obj', v4, faces);