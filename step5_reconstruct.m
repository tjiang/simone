function step5_reconstruct
load('landmarks', 'landmarks');

load('vertex', 'vertex');
load('faces', 'faces');

vertex = vertex';

v5 = zeros(size(vertex));


%vertex = vertex';
%faces = faces';

%plot_mesh(vertex, faces);
%scatter3(vertex(1,landmarks),vertex(2,landmarks),vertex(3,landmarks),20,[1 0 0],'filled');

load('v4','v4');
v4 = v4';
load('coarse_faces','coarse_faces');
%plot_mesh(v4, coarse_faces);



load('Dland', 'Dland');
[D,I] = sort(Dland,2);



K = 8;
I_nearest = I(:, 1:K);% #n * K  
K_nearest = landmarks(I_nearest);                                                                                                                                                         


%calculate weights
W_nearest = (1 -  D(:, 1:K) ./ repmat(D(:,K+1), 1, K)) .^ 2 ;
W_nearest = W_nearest ./ repmat(sum(W_nearest , 2), 1, K);


load('S4','S4');
load('R4','R4');


nvertices = size(vertex,2);

%reconstruction
for i = 1:nvertices
    if ~ismember(i, landmarks)
        for j = 1:K
            R = R4(:,:,I_nearest(i,j));
            S = S4(I_nearest(i,j));
            x_diff = vertex(:,i)-vertex(:,K_nearest(i,j));
            v5(:,i) = v5(:,i) + W_nearest(i,j) * (S * R * x_diff + v4(:,I_nearest(i,j)));
        end
    else
        v5(:,i) = v4(:,I_nearest(i,1));
    end
end
v5 = v5';
plot_mesh(v5, faces);
colormap jet(256);
shading interp;
save('v5','v5');
end