function step6_fine_fitting

debug = 1;
b_cal = 1;
options = [];
step2_find_features(1,0,0.1,'interp');
M2(1) = getframe(gca);  

load('v5', 'v5');
v6 = v5;
faces = load('faces','faces');
faces = faces.faces;


[options] = my_plot_mesh(v6, faces, options);
M2(2) = getframe(gca);  

load('Target_vertex','Target_vertex');
load('Target_faces','Target_faces');


load('Template_features','Template_features');
load('Target_features','Target_features');

load('landmarks','landmarks');

Template_features = landmarks(Template_features);

BI = Template_features;
BC = Target_vertex(Target_features,:);

w_casap = 1000;%sqrt(1000);
w_c = 5;%sqrt(10);
w_f = 10000;
D_ = 0.05*norm(max(Target_vertex) - min(Target_vertex));
[Target_normal, ~] = compute_normal(Target_vertex, Target_faces);
Target_normal = Target_normal';
options.normal = Target_normal;
%plot_mesh(Target_vertex,Target_faces, options);



if b_cal
 Target_wks = compute_wks(Target_vertex, Target_faces);
 Target_sihks = compute_sihks(Target_vertex, Target_faces);
%  Target_wks = [];
%  Target_sihks = [];
[~,~,~,~,Target_mc,~,~] = compute_curvature(Target_vertex, Target_faces, options);
else
    Target_wks = [];
    Target_sihks = [];
    Target_mc = [];
end
% 
% 

options.curvature_smoothing = 10;
tau = 1.2;


w_position = 1;
w_normal = 1;
w_mean_curvature = 1;%有用
w_wks = 1;%没用
w_sihks = 1;%没用
w_match = 0;
w_smooth = 1;
Target_descriptor = [w_position*Target_vertex, w_normal*Target_normal, w_mean_curvature*Target_mc, w_wks*Target_wks, w_sihks*Target_sihks];

Target_neighbor = compute_vertex_ring(Target_faces);

debug_index = [ ]; % the indices of debug template vertices;

% shading interp; camlight; colormap jet(256);

options.h_normal = [];
options.show_normal = debug_index;
while(w_casap > 1)
    if b_cal
     Template_wks = compute_wks(v6, faces);
     Template_sihks = compute_sihks(v6, faces);
%         Template_wks = [];
%         Template_sihks = [];
     options.curvature_smoothing = 10;
    [~,~,~,~,Template_mc,~,~] = compute_curvature(v6, faces, options);
    else
        Template_wks = [];
        Template_sihks = [];
        Template_mc = [];
    end
   
    [v6_normal, ~] = compute_normal(v6, faces);
    v6_normal = v6_normal';
    
    Template_descriptor = [w_position*v6, w_normal*v6_normal, w_mean_curvature*Template_mc, w_wks*Template_wks, w_sihks*Template_sihks];
    
    hold on
    options.normal = v6_normal;
    
    if ~isempty(options.h_normal)
        delete(options.h_normal)
    end
    [options] = my_plot_mesh(v6, faces, options);
    %alpha(options.hpatch, 0.1);
    %colormap jet(256);
    hold off
    IDX_TARGET = [];
    IDX_TEMPLATE = [];
    IDX_TARGET2 = [];
    IDX_TEMPLATE2 = [];
    if w_casap > 30 %以最近点开始找匹配点
    
        [IDX,~] = knnsearch(Target_vertex, v6, 'k',1, 'NSMethod', 'kdtree');%Target_vertex(IDX,:) 为离v4最近的点

        b = sqrt(sum(abs(Target_vertex(IDX,:) - v6).^2,2)) < D_ & dot(Target_normal(IDX,:),v6_normal ,2) > 0 ;%IDX2为500最近pairs中符合条件的pairs
        IDX2 = find(b);

        IDX_TARGET = IDX(IDX2);
        IDX_TEMPLATE = IDX2;
        
    else %法向量要求变高
        [IDX,~] = knnsearch(Target_vertex, v6, 'k',1, 'NSMethod', 'kdtree');%Target_vertex(IDX,:) 为离v4最近的点

        b = sqrt(sum(abs(Target_vertex(IDX,:) - v6).^2,2)) < D_ & dot(Target_normal(IDX,:),v6_normal ,2) > 0;%IDX2为500最近pairs中符合条件的pairs
        IDX2 = find(b);

        IDX_TARGET = IDX(IDX2);
        IDX_TEMPLATE = IDX2;
        if debug
            num = size(IDX_TARGET, 1); 
            colorvector = (1:num)'/ num; 
            hold on
            if exist('h11')~=0
                delete(h11);  
            end
            h11 = scatter3(v6(IDX_TEMPLATE,1),v6(IDX_TEMPLATE,2),v6(IDX_TEMPLATE,3),20,colorvector); 
            if exist('h22')~=0
                delete(h22);
            end
            h22 = scatter3(Target_vertex(IDX_TARGET,1),Target_vertex(IDX_TARGET,2),Target_vertex(IDX_TARGET,3),20,colorvector,'filled'); 
            colormap jet(256);
            delete(h11);
            delete(h22);
            
        end
    end
%         [IDX,~] = knnsearch(Target_vertex, v4, 'k',1, 'NSMethod', 'kdtree');%Target_vertex(IDX,:) 为离v4最近的点
% 
%         b = sqrt(sum(abs(Target_vertex(IDX,:) - v4).^2,2)) < D_ & dot(Target_normal(IDX,:),v4_normal ,2) > 0 ;%IDX2为500最近pairs中符合条件的pairs
%         IDX1 = find(b);
%     
%         
%         descriptor_dis = normrow(Target_descriptor(IDX(IDX1),:) - Template_descriptor(IDX1,:));
%         IDX2 = find(descriptor_dis<0.1)
%         
%         
%         IDX_TARGET = IDX(IDX1(IDX2));
%         IDX_TEMPLATE = IDX1(IDX2);
%         Dc  = repmat(dot(Target_vertex(IDX_TARGET,:) - v4(IDX_TEMPLATE,:), v4_normal(IDX_TEMPLATE,:),2),1,3).*v4_normal(IDX_TEMPLATE,:)*1 + v4(IDX_TEMPLATE,:);
%         
% 
    
%         
%                 
%         IDX3 = setdiff(IDX1,IDX1(IDX2));%IDX3为需要补充的匹配点
%         
%         radius = 0.2;
%         for i = 1:size(IDX3,1)
%             dis = normrow(Target_vertex-repmat(v4(IDX3(i),:),size(Target_vertex,1),1));
%             IDX4 = find(dis < radius);%IDX4为半径内的目标点
%             if debug & ismember(i, debug_index)% show target vertices in sphere
%                 [x,y,z] = sphere();
%                 hold on
%                 h = surf( radius*x+v4(IDX3(i),1), radius*y+v4(IDX3(i),2), radius*z+v4(IDX3(i),3),'EdgeAlpha',0,'FaceColor', 'interp' );
%                 alpha(h,0.1);
%                 if exist('h1')~=0
%                     delete(h1);
%                 end
%                 h1 = scatter3(Target_vertex(IDX4,1),Target_vertex(IDX4,2),Target_vertex(IDX4,3),20,'filled'); 
%                 delete(h1);
%                 delete(h);
%                 
%                 hold off
%             end
%             %show feature distance 
%             
%             descriptor_dis = normrow(Target_descriptor(IDX4,:) - repmat(Template_descriptor(IDX3(i),:),size(IDX4,1),1));
%             descriptor_dis = rescale(descriptor_dis);
%             [descriptor_dis1, IDX5] = sort(descriptor_dis);%找前15个feature最接近的目标点IDX5
%             IDX5 = IDX5(1:15);
%             if debug & ismember(i, debug_index)
%                 if exist('h2')~=0
%                     delete(h2);
%                 end
%                 hold on
%                 %h2 = scatter3(Target_vertex(IDX(IDX2(1:15)),1),Target_vertex(IDX(IDX2(1:15)),2),Target_vertex(IDX(IDX2(1:15)),3),20,descriptor_dis(1:15),'filled');
%                 h2 = scatter3(Target_vertex(IDX4,1),Target_vertex(IDX4,2),Target_vertex(IDX4,3),20,descriptor_dis,'filled'); 
%                 colormap jet(256);
%                 delete(h2);
%                 hold off
%             end
%             cos_ = dot(Target_normal(IDX4(IDX5),:), repmat(v4_normal(IDX3(i),:),size(IDX5,1),1), 2);
%             IDX6 = find ( cos_ > 0.7);
%             if debug & ismember(i, debug_index)
%                 if exist('h3')~=0
%                     delete(h3);
%                 end
%                 hold on
%                 h3 = scatter3(Target_vertex(IDX4(IDX5(IDX6)),1),Target_vertex(IDX4(IDX5(IDX6)),2),Target_vertex(IDX4(IDX5(IDX6)),3),20,cos_(IDX6),'filled'); 
%                 delete(h3);
%                 hold off
%             end
%             [~,IDX7] = max(cos_(IDX6));
%             if debug & ismember(i, debug_index)
%                 if exist('h4')~=0
%                     delete(h4);
%                 end
%                 hold on
%                 h4 = scatter3(Target_vertex(IDX4(IDX5(IDX6(IDX7))),1),Target_vertex(IDX4(IDX5(IDX6(IDX7))),2),Target_vertex(IDX4(IDX5(IDX6(IDX7))),3),20,'filled'); 
%                 delete(h4);
%                 hold off
%             end
%             if ~isempty(IDX7)
%                 IDX_TEMPLATE2(end+1) = IDX3(i);
%                 IDX_TARGET2(end+1) = IDX4(IDX5(IDX6(IDX7)));
%             end
%         end
%         IDX_TEMPLATE2 = IDX_TEMPLATE2';
%         IDX_TARGET2 = IDX_TARGET2';
%         IDX_TEMPLATE = [IDX_TEMPLATE; IDX_TEMPLATE2];
%         IDX_TARGET = [IDX_TARGET; IDX_TARGET2];
%         Dc  = [Dc; Target_vertex(IDX_TARGET2,:)];
%         
%         if debug
%             num = size(IDX_TARGET2, 1); 
%             colorvector = (1:num)'/ num; 
%             hold on
%             if exist('h11')~=0
%                 delete(h11);  
%             end
%             h11 = scatter3(v4(IDX_TEMPLATE2,1),v4(IDX_TEMPLATE2,2),v4(IDX_TEMPLATE2,3),20,colorvector); 
% 
% 
%             if exist('h22')~=0
%                 delete(h22);  
%             end
%             h22 = scatter3(Target_vertex(IDX_TARGET2,1),Target_vertex(IDX_TARGET2,2),Target_vertex(IDX_TARGET2,3),20,colorvector,'filled'); 
%             colormap jet(256);
%             delete(h11);
%             delete(h22);
% 
%         end
%     end
    
%     
%     
%     for i = 1:size(IDX_TARGET,1)%以feature搜索
%        j = IDX_TARGET(i);
%        k = j;
%        while 1
%            j = k;
%            neighbors = [j Target_neighbor{IDX_TARGET(i)}];
%            n_neighbors = numel(neighbors);
%            d_neighbors = normrow(Target_descriptor(neighbors',:) - repmat(Template_descriptor(IDX_TEMPLATE(i),:),n_neighbors,1));
%            ave_d = sum(d_neighbors) / n_neighbors;
%            E_m = w_match * d_neighbors.^2 + w_smooth*(d_neighbors-repmat(ave_d,n_neighbors,1)).^2;
%            [~,I] = min(E_m,[],1);
%            k = neighbors(I);
%            if k == j
%                break
%            end
%        end
%        IDX_TARGET(i) = j;
%        if debug  & ismember(i, debug_index )
%            if exist('h5')~=0
%                delete(h5);
%            end
%            hold on
%            h5 = scatter3(Target_vertex(j,1),Target_vertex(j,2),Target_vertex(j,3),20,'filled');
%            delete(h5);
%            hold off
%        end
%     end
    
    Dc  = repmat(dot(Target_vertex(IDX_TARGET,:) - v6(IDX_TEMPLATE,:), v6_normal(IDX_TEMPLATE,:),2),1,3).*v6_normal(IDX_TEMPLATE,:)*1 + v6(IDX_TEMPLATE,:);
    
    
    if debug
        num = size(IDX_TARGET, 1); 
        colorvector = (1:num)'/ num; 
        hold on
        if exist('h11')~=0
            delete(h11);
        end
        h11 = scatter3(v6(IDX_TEMPLATE,1),v6(IDX_TEMPLATE,2),v6(IDX_TEMPLATE,3),20,colorvector);  

        if exist('h22')~=0
            delete(h22);
        end
        h22 = scatter3(Target_vertex(IDX_TARGET,1),Target_vertex(IDX_TARGET,2),Target_vertex(IDX_TARGET,3),20,colorvector,'filled'); 
        %colormap jet(256);
        delete(h11);
        delete(h22);
    end

    
   
    
   
    
    i = 1:size(Template_features,2);
    j = Template_features;
    v = ones(size(Template_features));
    Cf = full(sparse(i,j,v,size(Template_features,2), size(v6,1)));
    Df = Target_vertex(Target_features,:);

    ii = 1:size(IDX_TEMPLATE,1);
    jj = IDX_TEMPLATE;
    vv = ones(size(IDX_TEMPLATE));
    Cc = full(sparse(ii,jj,vv,size(IDX_TEMPLATE,1), size(v6,1)));
    
    
    v6= asap_2(v6,faces,BI,BC,Cf,Df,Cc,Dc,w_casap,w_f,w_c,'Energy','spokes-and-rims','Smooth','true');
    

   if( w_casap > 100)
        w_casap = floor(w_casap / 1.1);
   elseif( w_casap > 10)
        w_casap = w_casap - 5;
   else
       w_casap = w_casap - 0.5;
   end
    
    %w_casap = w_casap - 0.01;
    M2(end+1) = getframe(gca);
    fprintf('w_casap = %d\n',  w_casap);
end
plot_mesh(v6, faces);
save('v6','v6');
save('M2','M2');
