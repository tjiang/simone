syms p1x p2x p3x q1x q2x q3x cot1 cot2 cot3

R = [3*cot3+3*cot2 -3*cot3 -3*cot2;...
     -3*cot3 3*cot3+3*cot1 -3*cot1;...
     -3*cot2 -3*cot1 3*cot1+3*cot2;];
P = [p1x; p2x; p3x];
Q = [q1x; q2x; q3x];


trace(Q' * R * P)